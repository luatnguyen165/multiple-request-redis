const Redis = require("ioredis");
const redis = new Redis();

module.exports = {
    set: (keyName,value)=> redis.set(keyName,value),
    get: (keyName)=> redis.get(keyName),
    setnx: (keyName,value,ttl)=> redis.set(keyName,value,'EX',ttl),
    incrby: (keyName,number)=> redis.incrby(keyName,number),
    exists: (keyName)=> redis.exists(keyName)
}